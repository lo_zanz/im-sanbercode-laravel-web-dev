<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");
echo "Name : ". $sheep->name . "<br>"; // "shaun"
echo "legs : ". $sheep->legs . "<br>"; // 4
echo  "cold blooded : ". $sheep->cold_blooded . "<br><br>"; // "no"

$frog = new frog("buduk");
echo "Name : ". $frog->name . "<br>"; // "shaun"
echo "legs : ". $frog->legs . "<br>"; // 4
echo  "cold blooded : ". $frog->cold_blooded . "<br>"; // "no"
echo "jump : " . $frog->jump([2," kali"]);

$ape = new ape("kera sakti");
echo "Name : ". $ape->name . "<br>"; // "shaun"
echo "legs : ". $ape->legs . "<br>"; // 4
echo  "cold blooded : ". $ape->cold_blooded . "<br>"; // "no"
echo "yell : " . $ape->yell();

?>