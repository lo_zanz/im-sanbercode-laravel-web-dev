<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'bio']);
Route::get('/pendaftaran', [AuthController::class,'daftar']);
Route::post('/kirim', [AuthController::class,'sign']);

Route::get('/table', function(){
    return view('page.table');
});
Route::get('/data-table', function(){
    return view('page.data-table');
});


//testing master template
// Route::get('/master',function(){
//     return view('layouts.master');
// });

//crud
//route ke form cast
Route::get('/cast/create',[CastController::class,'create']);
//route ke database cast
Route::post('/cast',[CastController::class,'homecast']);

//read data:::route ke halaman tampil data cast
Route::get('/cast',[CastController::class,'index']);
//route detail cast berdasarkan id
Route::get('/cast/{id}',[CastController::class,'show']);
//route ke form edit cast
Route::get('/cast/{id}/edit',[CastController::class,'edit']);
Route::put('/cast/{id}',[CastController::class,'update']);
//delete data
Route::delete('/cast/{id}',[CastController::class,'destroy']);