<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.register');
    }

    public function sign(Request $request)
    {
        // dd($request->all());
        $awal = $request['namaawal'];
        $akhir =$request['namaakhir'];
        
        return view('page.welcome',['namaawal'=>$awal, 'namaakhir'=>$akhir]);
    }
}
