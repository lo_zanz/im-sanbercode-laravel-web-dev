<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }
    public function homecast(Request $request)
    {
        $request->validate([
            'namacast' => 'required',
            'umurcast' => 'required',
            'biocast' => 'required',
        ],
    [
        'namacast.required' => 'nama harus diisi cuy',
        'umurcast.required' => 'umur harus diisi cuy',
        'biocast.required' => 'bio harus diisi cuy'
    ]);
        DB::table('cast')->insert([
            'nama' => $request['namacast'],
            'umur' => $request['umurcast'],
            'bio' => $request['biocast']
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $varcast = DB::table('cast')->get();
        // dd($cast);

        return view('cast.tampil', ['varcast' => $varcast]);
    }
    public function show($id)
    {
        $varcast = DB::table('cast')->find($id);
        return view('cast.detail',['varcast' => $varcast]);
    }
    public function edit($id)
    {
        $varcast = DB::table('cast')->find($id);
        return view('cast.edit',['varcast'=>$varcast]);   
    }
    public function update($id,Request $request)
    {
        $request->validate([
            'namacast' => 'required',
            'umurcast' => 'required',
            'biocast' => 'required',
        ],
    [
        'namacast.required' => 'nama harus diisi cuy',
        'umurcast.required' => 'umur harus diisi cuy',
        'biocast.required' => 'bio harus diisi cuy'
    ]);
    DB::table('cast')
              ->where('id', $id)
              ->update(
                ['nama' => $request['namacast'],
                'umur' => $request['umurcast'],
                'bio' => $request['biocast']
                ]
            );
            return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
