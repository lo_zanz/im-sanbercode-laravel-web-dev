@extends('layouts.master')
@section('title')
    Halaman Registrasi
@endsection

@section('sub-title')
    Registrasi
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/kirim" method="post">
    @csrf
    <label>First name :</label><br><br>
    <input type="text" name="namaawal" id=""><br><br>
    <label>Last name :</label><br><br>
    <input type="text" name="namaakhir" id=""><br><br>
    <label>Gender :</label><br><br>
        <input type="radio" name="gender" id="" value="1">Male<br>
        <input type="radio" name="gender" id="" value="2">Female<br>
        <input type="radio" name="gender" id="" value="3">Other<br><br>
    <label>Nationality :</label><br><br>
    <select name="nationality" id="">
        <option >Pilih</option>
        <option value="1">Indonesia</option>
        <option value="2">Malaysia</option>
        <option value="3">Singapura</option>
        <option value="4">Thailand</option>
        <option value="5">Vietnam</option>
        <option value="6">Kamboja</option>
    </select><br><br>
    <label>Language Spoken :</label> <br><br>
        <input type="checkbox" name="bahasa" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" value="2">English <br>
        <input type="checkbox" name="bahasa" value="3">Arabic <br>
        <input type="checkbox" name="bahasa" value="4">Other <br><br>
    <label>Bio :</label><br><br>
    <textarea name="" id="" cols="50" rows="5"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>
@endsection
    