@extends('layouts.master')
@section('title')
    Halaman Cast
@endsection
@section('sub-title')
    Tambah Cast
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Cast</label>
      <input type="text" name="namacast" class="form-control">
    </div>
    @error('namacast')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umurcast" class="form-control">
    </div>
    @error('umurcast')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="biocast" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    @error('biocast')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection