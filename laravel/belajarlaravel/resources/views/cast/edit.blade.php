@extends('layouts.master')
@section('title')
    Halaman Edit Cast
@endsection
@section('sub-title')
    Edit Cast
@endsection
@section('content')
<form action="/cast/{{$varcast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama Cast</label>
      <input type="text" name="namacast" value="{{$varcast->nama}}" class="form-control">
    </div>
    @error('namacast')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umurcast" value="{{$varcast->umur}}" class="form-control">
    </div>
    @error('umurcast')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="biocast" class="form-control" id="" cols="30" rows="10">{{$varcast->bio}}</textarea>
    </div>
    @error('biocast')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection