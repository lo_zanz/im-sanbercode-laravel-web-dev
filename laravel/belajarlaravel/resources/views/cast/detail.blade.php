@extends('layouts.master')
@section('title')
    Halaman Detail Cast
@endsection
@section('sub-title')
    Cast
@endsection
@section('content')
    <h1>{{$varcast->nama}}</h1>
    <p>{{$varcast->umur}}</p>
    <p>{{$varcast->bio}}</p>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection